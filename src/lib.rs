use proc_macro_hack::proc_macro_hack;
use std::marker::PhantomData;

#[proc_macro_hack]
pub use tiny_dom_macro::dom;

#[doc(hidden)]
pub mod internal;

pub type DomElement = wasm_bindgen::JsValue;

pub struct TemplateOutput<E, R> {
    pub element: DomElement,
    pub refs: R,
    _p: PhantomData<E>,
}

#[doc(hidden)]
impl<E> TemplateOutput<E, ()> {
    pub fn create(element: DomElement) -> Self {
        TemplateOutput {
            element,
            refs: (),
            _p: PhantomData,
        }
    }
}

impl<E, R> TemplateOutput<E, R> {
    pub fn on_event(&self, handler: impl FnMut(E)) {
        // TODO
    }
}

pub trait Insertable: internal::templating::TemplMulti { }
pub trait Templatable: Insertable + internal::templating::TemplText { }
