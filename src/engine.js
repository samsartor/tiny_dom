let stack = [];
let dec = new TextDecoder('utf-8');
let buffer;
let mem;

export function set_mem(memory) {
  if (memory.buffer !== buffer) {
    buffer = memory.buffer;
    mem = new DataView(memory.buffer);
  }
}

export function peek() {
  if (stack.length == 0) {
    return null;
  }
  return stack[stack.length - 1];
}

export function push(val) {
  return stack.push(val);
}

export function push_str(start, len) {
  return stack.push(dec.decode(buffer.slice(start, start + len)));
}

function pop() {
  return stack.pop();
}

function to_child(el) {
  const par = peek();
  if (par != null) {
    par.appendChild(el);
  }
}

export function evaluate(callback, text_begin, code_begin, code_end) {
  stack = [];
  let inst = 0;

  while (true) {
    const at = code_begin + inst;
    if (at == code_end) {
      return;
    }
    inst += 1;
    switch (mem.getInt8(at)) {
      // Callback to Rust.
      case 0:
        inst = callback(inst);
        continue;

      // Push string literal.
      //   bytes 2-5 = start offset
      //   bytes 5-9 = length
      case 1:
        inst += 8;
        push_str(
          text_begin + mem.getUint32(at + 1, true),
          mem.getUint32(at + 5, true));
        continue;

      // Push and append element.
      //   stack 0: node type
      //   stack 1 (peek): parent
      case 2:
        const el = document.createElement(pop());
        to_child(el);
        push(el);
        continue;

      // Pop stack top.
      case 3:
        pop();
        continue;

      // Set attribute.
      //   stack 0: key
      //   stack 1: value
      case 4:
        const val = pop();
        const key = pop();
        peek().setAttribute(key, val);
        continue;

      // Pop and append element.
      //   stack 0: element or text
      //   stack 1 (peek): parent
      case 5:
        let text = pop();
        if (text != null) {
          if (typeof text === 'string') {
            text = document.createTextNode(text);
          }
          to_child(text);
        }
        continue;

      // Push selected.
      //   stack 0: CSS selector
      case 6:
        push(document.querySelector(pop()));
    }
  }
}
