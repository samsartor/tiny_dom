use proc_macro2::Span;
use syn::{LitByteStr, Expr};
use byteorder::{WriteBytesExt, LE};
use quote::{quote, ToTokens};

pub struct Builder {
    text: String,
    instructions: Vec<u8>,
    callbacks: Vec<(usize, Callback)>,
    depth: usize,
}

pub enum Callback {
    PushExprAsText(Expr),
    PopAsOutputEl,
}

impl Builder {
    pub fn new() -> Builder {
        Builder {
            text: String::new(),
            instructions: Vec::new(),
            callbacks: Vec::new(),
            depth: 0,
        }
    }

    fn i_callback(&mut self, callback: Callback) {
        self.instructions.write_u8(0).unwrap();
        self.callbacks.push((self.instructions.len(), callback));
    }

    fn i_push_text(&mut self, text: &str) {
        self.instructions.write_u8(1).unwrap();
        let start = match self.text.find(text) {
            Some(i) => i,
            None => {
                let start = self.text.len();
                self.text += text;
                start
            }
        };
        self.instructions.write_u32::<LE>(start as u32).unwrap();
        self.instructions.write_u32::<LE>(text.len() as u32).unwrap();
    }

    fn i_push_and_append_element(&mut self) {
        self.instructions.write_u8(2).unwrap();
    }

    fn i_pop(&mut self) {
        self.instructions.write_u8(3).unwrap();
    }

    fn i_set_attr(&mut self) {
        self.instructions.write_u8(4).unwrap();
    }

    fn i_pop_and_append_element(&mut self) {
        self.instructions.write_u8(5).unwrap();
    }

    fn i_push_selected(&mut self) {
        self.instructions.write_u8(6).unwrap();
    }

    pub fn select_element(&mut self, query: &str) {
        println!("< INTO {:?} >", query);
        self.depth += 1;
        self.i_push_text(query);
        self.i_push_selected();
    }

    pub fn begin_element(&mut self, kind: &str) {
        println!("<{}>", kind);
        self.depth += 1;
        self.i_push_text(kind);
        self.i_push_and_append_element();
    }

    pub fn text(&mut self, text: &str) {
        println!("{:?}", text);
        self.i_push_text(text);
        self.i_pop_and_append_element();
    }

    pub fn expr(&mut self, val: Expr) {
        println!("{:?}", val);
        // TODO: support non-text nodes
        self.i_callback(Callback::PushExprAsText(val));
        self.i_pop_and_append_element();
    }

    pub fn attr_text(&mut self, key: &str, val: &str) {
        println!("{}={:?}", key, val);
        self.i_push_text(key);
        self.i_push_text(val);
        self.i_set_attr();
    }

    pub fn attr_expr(&mut self, key: &str, val: Expr) {
        println!("{}={:?}", key, val);
        self.i_push_text(key);
        self.i_callback(Callback::PushExprAsText(val));
        self.i_set_attr();
    }

    pub fn end_element(&mut self) {
        println!("</>");
        match self.depth {
            0 => panic!("no element to end"),
            1 => self.i_callback(Callback::PopAsOutputEl),
            _ => (),
        }
        self.depth -= 1;
        self.i_pop();
    }

    pub fn into_rust(self) -> impl ToTokens {
        use Callback::*;

        let text = self.text;
        let code = LitByteStr::new(&self.instructions, Span::call_site());
        let calls = self.callbacks.iter().map(|(state, cb)| match cb {
            PushExprAsText(x) => quote! {
                #state => {
                    let x = #x;
                    ::tiny_dom::internal::push_str(::tiny_dom::internal::templating::TemplText::write_to_str(&x, &mut BUFFER));
                }
            },
            PopAsOutputEl => quote! {
                #state => OUTPUT_EL = ::tiny_dom::internal::peek_top()
            },
        });
        quote! { {
            static TEXT: &str = #text;
            static CODE: &[u8] = #code;
            let mut BUFFER = String::new();
            let mut OUTPUT_EL = ::tiny_dom::DomElement::NULL;
            ::tiny_dom::internal::evaluate(|next_i| {
                match next_i {
                    #(#calls,)*
                    _ => (),
                }
                next_i
            }, TEXT, CODE);
            ::tiny_dom::TemplateOutput::create(OUTPUT_EL)
        } }
    }
}
