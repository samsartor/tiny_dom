extern crate proc_macro;

use proc_macro_hack::proc_macro_hack;
use proc_macro::{TokenStream};
use quote::ToTokens;
use tiny_dom_compiler::Builder;

use syn::parse::{Parse, ParseStream, Result as SynResult};
use syn::{
    parenthesized,
    parse_macro_input,
    parse_quote,
    Ident,
    LitStr,
    Expr,
    Pat,
    punctuated::Punctuated,
    token
};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum CompilerPass {
    BeforeEl,
    InsideEl,
    AfterEl,
}

use CompilerPass::*;

#[proc_macro_hack]
pub fn dom(stream: TokenStream) -> TokenStream {
    let items = parse_macro_input!(stream as ItemList);
    // println!("{:#?}", items);
    let mut builder = Builder::new();
    items.compile(&mut builder, BeforeEl);
    items.compile(&mut builder, InsideEl);
    items.compile(&mut builder, AfterEl);
    builder.into_rust().into_token_stream().into()
}

#[derive(Debug)]
struct ItemList(Vec<Item>);

impl Parse for ItemList {
    fn parse(input: ParseStream) -> SynResult<Self> {
        Ok(ItemList(Punctuated::<Item, token::Comma>::parse_terminated(input)?
            .into_iter()
            .collect()))
    }
}

impl ItemList {
    fn compile(&self, into: &mut Builder, pass: CompilerPass) {
        match pass {
            AfterEl => for i in self.0.iter().rev() {
                i.compile(into, pass);
            },
            _ => for i in &self.0 {
                i.compile(into, pass);
            },
        }

    }
}

#[derive(Debug)]
enum Item {
    Element(Ident, token::Paren, ItemList),
    AttrText(Ident, token::Eq, LitStr),
    AttrExpr(Ident, token::Eq, Expr),
    NodeText(LitStr),
    NodeExpr(Expr),
    MountIn(token::In, LitStr),
    OnEvent(token::At, Punctuated<Ident, token::Dot>, token::Eq, Expr),
    IfExpr(token::If, Expr),
    ForExpr(token::For, Pat, token::In, Expr),
}

fn tagify(kind: &mut String) {
    kind.make_ascii_uppercase();
}

fn attrify(key: &Ident) -> String {
    key.to_string().replace('_', "-")
}

impl Item {
    fn compile(&self, into: &mut Builder, pass: CompilerPass) {
        use Item::*;

        match (self, pass) {
            (Element(kind, _, li), InsideEl) => {
                let mut kind = kind.to_string();
                tagify(&mut kind);
                li.compile(into, BeforeEl);
                into.begin_element(&kind);
                li.compile(into, InsideEl);
                into.end_element();
                li.compile(into, AfterEl);
            },
            (AttrText(key, _, val), InsideEl) => into.attr_text(&attrify(key), &val.value()),
            (AttrExpr(key, _, val), InsideEl) => into.attr_expr(&attrify(key), val.clone()),
            (NodeText(lit), InsideEl) => into.text(&lit.value()),
            (MountIn(_, query), BeforeEl) => into.select_element(&query.value()),
            (MountIn(_, _), AfterEl) => into.end_element(),
            _ => (),
        }
    }
}

impl Parse for Item {
    fn parse(input: ParseStream) -> SynResult<Self> {
        use Item::*;
        Ok(if input.peek(Ident) {
            let prefix = input.parse::<Ident>()?;
            if input.peek(token::Eq) {
                let eq = input.parse()?;
                if input.peek(LitStr) {
                    AttrText(prefix, eq, input.parse()?)
                } else {
                    AttrExpr(prefix, eq, input.parse()?)
                }
            } else if input.peek(token::Paren) {
                let content;
                Element(prefix, parenthesized!(content in input), content.parse()?)
            } else {
                NodeExpr(parse_quote! { #prefix })
            }
        } else if input.peek(token::In) {
            MountIn(input.parse()?, input.parse()?)
        } else if input.peek(token::If) {
            IfExpr(input.parse()?, input.parse()?)
        } else if input.peek(token::For) {
            ForExpr(input.parse()?, input.parse()?, input.parse()?, input.parse()?)
        } else if input.peek(token::At) {
            OnEvent(input.parse()?, Punctuated::parse_separated_nonempty(input)?, input.parse()?, input.parse()?)
        } else if input.peek(LitStr) {
            NodeText(input.parse()?)
        } else {
            NodeExpr(input.parse()?)
        })
    }
}
