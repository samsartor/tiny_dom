use wasm_bindgen::{memory, JsValue};

pub mod templating;

mod engine_sys {
    use wasm_bindgen::prelude::*;

    #[wasm_bindgen(module = "/src/engine.js")]
    extern "C" {
        pub fn set_mem(mem: JsValue);
        pub fn peek() -> JsValue;
        pub fn push(val: JsValue);
        pub fn push_str(start: *const u8, len: usize);
        pub fn evaluate(
            callback: &mut dyn FnMut(usize) -> usize,
            text_begin: *const u8,
            code_begin: *const u8,
            code_end: *const u8,
        );
    }
}

pub fn evaluate(mut callback: impl FnMut(usize) -> usize, text: &str, code: &[u8]) {
    // TODO: set mem less eagerly
    engine_sys::set_mem(memory());
    engine_sys::evaluate(
        &mut |i| {
            let i = callback(i);
            engine_sys::set_mem(memory());
            i
        },
        text.as_ptr(),
        code.as_ptr(),
        unsafe { code.as_ptr().offset(code.len() as isize) });
}

pub fn push_str(s: &str) {
    engine_sys::set_mem(memory());
    engine_sys::push_str(s.as_ptr(), s.len());
}

pub fn push_val(v: JsValue) {
    engine_sys::push(v);
}

pub fn peek_top() -> JsValue {
    engine_sys::peek()
}
