use std::fmt;
use std::rc::Rc;
use std::sync::Arc;
use crate::{Templatable, Insertable, DomElement};

pub trait TemplText {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error>;

    fn write_to_str<'a>(&'a self, buf: &'a mut String) -> &'a str {
        use std::fmt::Write;

        struct Displayer<'a, T: ?Sized>(&'a T);

        impl<'a, T: TemplText + ?Sized> fmt::Display for Displayer<'a, T> {
            fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
                self.0.fmt(f)
            }
        }

        write!(buf, "{}", Displayer(self)).expect("could not format item");
        buf.as_str()
    }
}

pub trait TemplDom {
    fn node(&self) -> &DomElement;
}

pub enum Never { }
impl TemplText for Never {
    fn fmt(&self, _: &mut fmt::Formatter) -> Result<(), fmt::Error> { unreachable!() }
    fn write_to_str<'a>(&'a self, _: &'a mut String) -> &'a str { unreachable!() }
}
impl TemplDom for Never {
    fn node(&self) -> &DomElement { unreachable!() }
}

pub enum TemplVar<A, B> {
    Text(A),
    Dom(B),
}

pub trait TemplMulti {
    type Text: TemplText + ?Sized;
    type Dom: TemplDom + ?Sized;

    fn get(&self) -> TemplVar<&Self::Text, &Self::Dom>;
}

macro_rules! templ_for_ptr {
    ($(for <$g:ident> $t:ty => |$v:ident| $x:expr),*$(,)?) => {$(
        impl<$g: Insertable + ?Sized> TemplMulti for $t {
            type Text = <$g as TemplMulti>::Text;
            type Dom = <$g as TemplMulti>::Dom;

            #[inline]
            fn get(&self) -> TemplVar<&Self::Text, &Self::Dom> {
                let $v = self;
                TemplMulti::get($x)
            }
        }

        impl<$g: TemplText + ?Sized> TemplText for $t {
            #[inline]
            fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
                let $v = self;
                TemplText::fmt($x, f)
            }

            #[inline]
            fn write_to_str<'a>(&'a self, buf: &'a mut String) -> &'a str {
                let $v = self;
                TemplText::write_to_str($x, buf)
            }
        }

        impl<$g: Insertable> Insertable for $t { }
        impl<$g: Templatable> Templatable for $t { }
    )*};
}

impl TemplText for str {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        fmt::Display::fmt(self, f)
    }

    #[inline]
    fn write_to_str<'a>(&'a self, _: &'a mut String) -> &'a str {
        self
    }
}

impl TemplText for String {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        fmt::Display::fmt(self, f)
    }

    #[inline]
    fn write_to_str<'a>(&'a self, _: &'a mut String) -> &'a str {
        self.as_str()
    }
}

macro_rules! tmpls_text {
    ($($t:ty),*$(,)?) => {$(
        impl TemplMulti for $t {
            type Text = Self;
            type Dom = Never;

            #[inline]
            fn get(&self) -> TemplVar<&Self, &Never> {
                TemplVar::Text(self)
            }
        }

        impl Insertable for $t { }
        impl Templatable for $t { }
    )*};
}

tmpls_text!(str, String);

macro_rules! templ_for_display {
    ($($t:ty),*$(,)?) => {$(
        impl TemplText for $t {
            #[inline]
            fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
                fmt::Display::fmt(self, f)
            }
        }

        tmpls_text!($t);
    )*};
}

templ_for_ptr!(
    for<T> &T => |x| *x,
    for<T> &mut T => |x| *x,
    for<T> Box<T> => |x| Box::as_ref(x),
    for<T> Rc<T> => |x| Rc::as_ref(x),
    for<T> Arc<T> => |x| Arc::as_ref(x),
);

templ_for_display!(
    u8,
    i8,
    u16,
    i16,
    u32,
    i32,
    u64,
    i64,
    usize,
    isize,
    fmt::Arguments<'_>,
);

macro_rules! templ_for_dom {
    ($(for <$($g:ident),*> $t:ty => |$v:ident| $x:expr),*$(,)?) => {$(
        impl<$($g),*> TemplDom for $t {
            #[inline]
            fn node(&self) -> &DomElement {
                let $v = self;
                $x
             }
        }

        impl<$($g),*> TemplMulti for $t {
            type Text = Never;
            type Dom = Self;

            #[inline]
            fn get(&self) -> TemplVar<&Never, &Self> {
                TemplVar::Dom(self)
            }
        }

        impl<$($g),*> Insertable for $t { }
    )*};
}

templ_for_dom!(
    for<> DomElement => |x| x,
    for<E, R> crate::TemplateOutput<E, R> => |x| &x.element,
);
