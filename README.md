# Tiny DOM

A tiny DOM is a 5kB templating library for Rust in the browser.

```rust
let numbers = 0..100;
let show = true;
dom! {
    in "#root",
    ol(
        if show,
        class = "list",
        li(
            for num in numbers,
            class = "element",
            num,
        ),
    ),
    div(
        class = match show {
            true => "footer-visible",
            false => "footer-hidden",
        },
        "Hello, World!",
    ),
};
```

## Internals

`tiny_dom` compiles your templates to a stupidly simple stack-based bytecode.
When the templates are used, JS interprets the bytecode in memory.
