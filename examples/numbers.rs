use tiny_dom::{dom, TemplateOutput};

pub fn main() {
    let numbers = 0..100;
    let show = true;
    let _: TemplateOutput<(), ()> = dom! {
        in "#numbers-list",
        ol(
            if show,
            class = "list",
            li(
                for num in numbers,
                class = "element",
                num,
            ),
        ),
        div(
            class = match show {
                true => "footer-visible",
                false => "footer-hidden",
            },
            "Hello, World!",
        ),
    };
}
