use tiny_dom::dom;

pub fn main() {
    fn view(count: usize) {
        let rendered = dom!(
            in "#counter-root",
            div(
                class="app",
                button(
                    class="up",
                    @click=1,
                    "+1",
                ),
                input(
                    class="count",
                    value=match count {
                        0 => format!("Click the Button!"),
                        1 => format!("1 Time"),
                        n => format!("{} Times", n),
                    },
                ),
                button(
                    class="down",
                    @click=-1,
                    "-1",
                ),
            ),
        );
        rendered.on_event(|delta: isize| view(
            if delta < 0 {
                count.saturating_sub((-delta) as usize)
            } else {
                count.saturating_add(delta as usize)
            }
        ));
    }

    view(0);
    view(1);
    view(2);
}
