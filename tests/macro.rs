use wasm_bindgen_test::*;
use tiny_dom::{dom, TemplateOutput};

#[wasm_bindgen_test]
fn test_dom_examples() {
    let _: TemplateOutput<(), ()> = dom!();
    let _: TemplateOutput<(), ()> = dom!(div("hello"));
    let _: TemplateOutput<(), ()> = dom!(
        span(
            foo="bar",
            foo=3,
            match true {
                true => "hi",
                false => "there",
            },
        ),
        span(
            "text",
        ),
    );
}
