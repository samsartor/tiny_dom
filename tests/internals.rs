use wasm_bindgen_test::*;
use tiny_dom::internal::*;

wasm_bindgen_test::wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn test_push_str() {
    evaluate(
        |next_i| {
            assert_eq!(next_i, 1);
            push_str("hello");
            assert_eq!(peek_top().as_string().as_ref().map(String::as_str), Some("hello"));
            next_i
        },
        "",
        &[0]);
}

#[wasm_bindgen_test]
fn test_evaluate_callback() {
    let mut success = false;
    evaluate(
        |next_i| {
            assert_eq!(next_i, 1);
            success = true;
            next_i
        },
        "",
        &[0]);
    assert!(success);
}

#[wasm_bindgen_test]
fn test_evaluate_string() {
    let text = "Hello, World!";
    evaluate(
        |next_i| {
            assert_eq!(next_i, 10);
            assert_eq!(peek_top().as_string().as_ref().map(String::as_str), Some(text));
            next_i
        },
        text,
        &[
            1, 0,0,0,0, text.len() as u8,0,0,0,
            0
        ]);
}

#[wasm_bindgen_test]
fn test_evaluate_pop() {
    evaluate(
        |next_i| {
            match next_i {
                1 => push_str("hello"),
                3 => assert!(peek_top().is_null()),
                _ => panic!("bad instruction ptr: {}", next_i),
            }
            next_i
        },
        "",
        &[0, 3, 0]);
}
