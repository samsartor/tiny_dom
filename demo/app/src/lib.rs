use wasm_bindgen::prelude::*;

macro_rules! examples {
    ($($file:ident),*) => {$(
        mod $file {
            include!(concat!("../../../examples/", stringify!($file), ".rs"));
        }
    )*};
}

examples!(counter, numbers);

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn run() -> Result<(), JsValue> {
    set_panic_hook();

    counter::main();
    numbers::main();

    Ok(())
}

#[cfg(feature = "console_error_panic_hook")]
fn set_panic_hook() {
    console_error_panic_hook::set_once();
}


#[cfg(not(feature = "console_error_panic_hook"))]
fn set_panic_hook() { }
